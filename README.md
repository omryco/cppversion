# Cpp Version

This project is a c++ version module, generated at compilation time.

# How to Use

- Add this project to your superproject as a submodule.

- In the superproject's `CMakeLists.txt`, add
    ```cmake
    configure_file("path/to/version.hpp.in" "${PROJECT_BINARY_DIR}/generated/cppversion/version.hpp" ESCAPE_QUOTES)
    include_directories("${PROJECT_BINARY_DIR}/generated/cppversion")
    ```

- When calling the `cmake` command, add `VERSION` and `CONFIG` as CMake variables:
    ```bash
    $ cmake ... -DVERSION=<versionstring> -DCONFIG=<configstring>
    ```
  Alternatively, `VERSION` and `CONFIG` can be set inside `CMakeLists.txt`:
    ```cmake
    set(VERSION <versionstring>)
    set(CONFIG <configstring)
    configure_file("path/to/version.hpp.in" "${PROJECT_BINARY_DIR}/generated/cppversion/version.hpp" ESCAPE_QUOTES)
    include_directories("${PROJECT_BINARY_DIR}/generated/cppversion")
    ```
    